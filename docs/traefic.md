
https://www.omgthecloud.com/freqtrade-deploy-understanding-the-order-book/

- MULTIPLAS ESTRATEDIAS:
https://www.omgthecloud.com/freqtrade-quickly-deploy-your-bot-army/
- MAIS REFERENCIAS:
https://betterprogramming.pub/traefik-2-1-as-a-reverse-proxy-c9e274da0a32

Implantação do FreqTrade, entendendo o Livro de Pedidos
Neste episódio, estamos implantando o FreqTrade, um bot de negociação CryptoCurrency de código aberto, envolvendo-o em uma bela interface da web com várias guias chamada Muximux e gerenciando tudo com o proxy da Web Traefik com certificados SSL reais de renovação automática da Let's Encrypt! Também mostrarei as noções básicas de configuração, como é um arquivo de estratégia simples e como funcionam os livros de pedidos de troca de criptomoedas. Confira! Após o vídeo, eu tenho seu código de exemplo abaixo!

Reprodutor do YouTube
Vamos começar com um docker-compose.yml básico para sua instância FreqTrade:
```yml
version: '3.6'

networks:
  proxy:
    external: true

services:
  freqtrade:
    image: freqtradeorg/freqtrade:develop
    networks:
      - proxy
    volumes:
      - "./user_data:/freqtrade/user_data"
      - /etc/timezone:/etc/timezone:ro
    # Default command used when running `docker compose up`
    command: >
      trade
      --logfile /freqtrade/user_data/logs/freqtrade.log
      --config /freqtrade/user_data/config.json
      --strategy BinHV45
      --db-url sqlite:///user_data/tradesv3.sqlite
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
      restart_policy:
        condition: on-failure
        delay: 5s
      labels:
         - 'traefik.enable=true'
         - 'traefik.http.routers.binhv45.tls=true'
         - 'traefik.http.routers.binhv45.rule=Host(`binhv45.dmz.yourdomain.com`)'
         - 'traefik.http.services.binhv45.loadbalancer.server.port=8080'

```
Se você ainda não tem um proxy Traefik configurado, você vai querer consultar meu vídeo e [código de exemplo para isso.](https://www.omgthecloud.com/traefik-v2-with-lets-encrypt-ssl/) Vai facilitar muito a sua vida!

Em seguida, vamos dar uma olhada como um arquivo config.json de amostra para FreqTrade . Isso varia de acordo com sua configuração, mas este é um bom ponto de partida:

```

{
    "max_open_trades": 25,
    "stake_currency": "USDT",
    "stake_amount": 50,
    "tradable_balance_ratio": 0.99,
    "fiat_display_currency": "USD",
    "timeframe": "1m",
    "dry_run": true,
    "cancel_open_orders_on_exit": false,
    "unfilledtimeout": {
        "buy": 10,
        "sell": 30
    },
    "bid_strategy": {
        "price_side": "bid",
        "ask_last_balance": 0.0,
        "use_order_book": false,
        "order_book_top": 1,
        "check_depth_of_market": {
            "enabled": false,
            "bids_to_ask_delta": 1
        }
    },
    "ask_strategy": {
        "price_side": "ask",
        "use_order_book": false,
        "order_book_min": 1,
        "order_book_max": 1,
        "use_sell_signal": true,
        "ignore_roi_if_buy_signal": true
    },
    "download_trades": true,
    "exchange": {
        "name": "binanceus",
        "key": "",
        "secret": "",
        "ccxt_config": {"enableRateLimit": true},
        "ccxt_async_config": {
            "enableRateLimit": true,
            "rateLimit": 200
        },
        "pair_whitelist": [
          "BTC/USDT",
          "ETH/USDT",
          "ETC/USDT",
          "LTC/USDT",
          "XLM/USDT",
          "ADA/USDT"
        ],
        "pair_blacklist": [
          "XRP/USD",
          "USDT/USD",
          "USDC/USD",
          "EUR/USD"
        ]
    },
    "pairlists": [
      {
//          "method": "StaticPairList"}
          "method": "VolumePairList",
          "number_assets": 50,
          "sort_key": "quoteVolume",
          "refresh_period": 1800
      },
      {"method": "AgeFilter", "min_days_listed": 10},
      {
        "method": "RangeStabilityFilter",
        "lookback_days": 5,
        "min_rate_of_change": 0.01,
        "refresh_period": 1440
      }
     ],

    "edge": {
        "enabled": false,
        "process_throttle_secs": 3600,
        "calculate_since_number_of_days": 7,
        "allowed_risk": 0.01,
        "minimum_winrate": 0.60,
        "minimum_expectancy": 0.20,
        "min_trade_number": 10,
        "max_trade_duration_minute": 1440,
        "remove_pumps": false
    },
    "telegram": {
        "enabled": false,
        "token": "",
        "chat_id": ""
    },
    "api_server": {
        "enabled": true,
        "enable_openapi": true,
        "listen_ip_address": "0.0.0.0",
        "listen_port": 8080,
        "verbosity": "info",
        "jwt_secret_key": "somethingrandom",
        "CORS_origins": [],
        "username": "api",
        "password": "api"
    },
    "initial_state": "running",
    "forcebuy_enable": false,
    "internals": {
        "process_throttle_secs": 5
    }
}

```
Em seguida, você precisará de um arquivo de estratégia. Este é o exemplo que estamos usando no vídeo, mas também recomendo que você confira os exemplos na própria página do GitHub da FreqTrade . Abaixo está o BinHV45.py armazenado na pasta /user_data/strategies/ :
```
# --- Do not remove these libs ---
from freqtrade.strategy.interface import IStrategy
from typing import Dict, List
from functools import reduce
from pandas import DataFrame
import numpy as np
# --------------------------------

import talib.abstract as ta
import freqtrade.vendor.qtpylib.indicators as qtpylib


def bollinger_bands(stock_price, window_size, num_of_std):
    rolling_mean = stock_price.rolling(window=window_size).mean()
    rolling_std = stock_price.rolling(window=window_size).std()
    lower_band = rolling_mean - (rolling_std * num_of_std)

    return rolling_mean, lower_band


class BinHV45(IStrategy):
    minimal_roi = {
    #    "0": 0.0125
      "0": 0.99
    }

    stoploss = -0.05
    timeframe = '1m'
    trailing_stop = True
    trailing_only_offset_is_reached = True
    trailing_stop_positive_offset = 0.00375  # Trigger positive stoploss once crosses above this percentage
    trailing_stop_positive = 0.00175 # Sell asset if it dips down this much


    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        mid, lower = bollinger_bands(dataframe['close'], window_size=40, num_of_std=2)
        dataframe['mid'] = np.nan_to_num(mid)
        dataframe['lower'] = np.nan_to_num(lower)
        dataframe['bbdelta'] = (dataframe['mid'] - dataframe['lower']).abs()
        dataframe['pricedelta'] = (dataframe['open'] - dataframe['close']).abs()
        dataframe['closedelta'] = (dataframe['close'] - dataframe['close'].shift()).abs()
        dataframe['tail'] = (dataframe['close'] - dataframe['low']).abs()
        return dataframe

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                dataframe['lower'].shift().gt(0) &
                dataframe['bbdelta'].gt(dataframe['close'] * 0.008) &
                dataframe['closedelta'].gt(dataframe['close'] * 0.0175) &
                dataframe['tail'].lt(dataframe['bbdelta'] * 0.25) &
                dataframe['close'].lt(dataframe['lower'].shift()) &
                dataframe['close'].le(dataframe['close'].shift())
            ),
            'buy'] = 1
        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        no sell signal
        """
        dataframe.loc[:, 'sell'] = 0
        return dataframe


```

## Parte 2


### Traefik v2 com Lets Encrypt SSL
Neste episódio, estamos implantando o proxy Traefik com um certificado SSL curinga Let's Encrypt. Estamos usando validação de DNS, então o Traefik também não precisa ser acessível externamente! Confira o vídeo, e abaixo está o código de exemplo também! Não se esqueça de usar seu provedor de DNS (meu código de exemplo é voltado para AWS Route53) e seu domínio real. Aproveitar!

:



Em seguida, basta preencher o seu docker-compose.ymlcom o código de exemplo abaixo e editar para se adequar ao seu ambiente.
```yml
version: '3.6'

networks:
  proxy:
    driver: overlay
    attachable: true
    name: proxy

services:
  traefik:
    image: traefik:v2.4
    ports:
      - "80:80"
      - "443:443"
    networks:
      - proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./letsencrypt:/letsencrypt
    command:
      - --api.insecure=true
      - --api.dashboard=true
      - --serversTransport.insecureSkipVerify=true
      - --api.debug=true
      - --providers.docker=true
      - --providers.docker.swarmMode=true
      - --providers.docker.network=proxy
      - --providers.docker.exposedByDefault=true
      - "--providers.docker.defaultRule=Host(`{{ normalize .Name }}.dmz.yourdomain.com`)"
      - --entrypoints.web.address=:80
      - --entrypoints.websecured.address=:443
      - --entrypoints.web.http.redirections.entryPoint.to=websecured
      - --entrypoints.web.http.redirections.entryPoint.scheme=https
      - "--certificatesresolvers.le.acme.dnschallenge=true"
      - "--certificatesresolvers.le.acme.httpChallenge=false"
      - "--certificatesresolvers.le.acme.tlsChallenge=false"
      - "--certificatesresolvers.le.acme.dnschallenge.provider=route53"
      - "--certificatesresolvers.le.acme.email=info@yourdomain.com"
      - "--certificatesresolvers.le.acme.storage=/letsencrypt/acme.json"
      - "--certificatesresolvers.le.acme.httpChallenge.entryPoint=web"

    environment:
      - "AWS_ACCESS_KEY_ID=yourKeyGoesHere"
      - "AWS_SECRET_ACCESS_KEY=yourSecretGoesHere"

    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
      restart_policy:
        condition: on-failure
        delay: 5s
      labels:
        - 'traefik.enable=true'
        - 'traefik.http.routers.traefik.rule=Host(`traefik.dmz.yourdomain.com`)'
        - 'traefik.http.routers.traefik.tls=true'
        - 'traefik.http.routers.traefik.tls.certresolver=le'
        - 'traefik.http.routers.traefik.service=api@internal'
        - 'traefik.http.services.api.loadbalancer.server.port=8080'
        - 'traefik.http.routers.traefik.tls.domains[0].main=dmz.yourdomain.com'
        - 'traefik.http.routers.traefik.tls.domains[0].sans=*.dmz.yourdomain.com'
```
