# Personalização de Estratégia

#
Esta página explica como personalizar suas estratégias, adicionar novos indicadores e configurar regras de negociação.

Familiarize-se primeiro com os [fundamentos do Freqtrade](https://www.freqtrade.io/en/stable/bot-basics/) , que fornece informações gerais sobre como o bot opera.

### Instale um arquivo de estratégia personalizado:


Isso é muito simples. Copie e cole seu arquivo de estratégia no diretório
user_data/strategies.

Suponha que você tenha uma classe chamada AwesomeStrategyno arquivo AwesomeStrategy.py:

1. Mova seu arquivo para user_data/strategies(você deveria teruser_data/strategies/AwesomeStrategy.py

2. Inicie o bot com o parâmetro --strategy AwesomeStrategy(o parâmetro é o nome da classe)

`freqtrade trade --strategy AwesomeStrategy`

### Desenvolva sua própria estratégia:

O bot inclui um arquivo de estratégia padrão. Além disso, várias outras estratégias estão disponíveis no repositório de estratégias .

No entanto, você provavelmente terá sua própria ideia para uma estratégia. Este documento pretende ajudá-lo a desenvolver um para você.

Para começar, use freqtrade `new-strategy --strategy AwesomeStrategy`. Isso criará um novo arquivo de estratégia a partir de um modelo, que estará localizado em `user_data/strategies/AwesomeStrategy.py`.
***
### Anatomia de uma estratégia:
Um arquivo de estratégia contém todas as informações necessárias para construir uma boa estratégia:


- **Indicadores**
- **Regras de estratégia de compra**
- **Regras de estratégia de venda**
- **ROI mínimo recomendado**
- **Stoploss fortemente recomendado**

O bot também incluem uma estratégia de amostra chamado SampleStrategyvocê pode atualizar: `user_data/strategies/sample_strategy.py`. Você pode testá-lo com o parâmetro:`--strategy SampleStrategy`

Além disso, há um atributo chamado `INTERFACE_VERSION`, que define a versão da interface de estratégia que o bot deve usar. A versão atual é 2 - que também é o padrão quando não é definid explicitamente na estratégia.

Versões futuras exigirão que isso seja definido.

## Personalizar Indicadores:

As estratégias de compra e venda precisam de indicadores. Você pode adicionar mais indicadores estendendo a lista contida no método `populate_indicators()`de seu arquivo de estratégia.

Você só deve adicionar os indicadores utilizados em qualquer `populate_buy_trend()`, `populate_sell_trend()`ou para preencher um outro indicador, caso contrário, o desempenho pode sofrer.

amostra:

```
def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
    """
    Adds several different TA indicators to the given DataFrame

    Performance Note: For the best performance be frugal on the number of indicators
    you are using. Let uncomment only the indicator you are using in your strategies
    or your hyperopt configuration, otherwise you will waste your memory and CPU usage.
    :param dataframe: Dataframe with data from the exchange
    :param metadata: Additional information, like the currently traded pair
    :return: a Dataframe with all mandatory indicators for the strategies
    """
    dataframe['sar'] = ta.SAR(dataframe)
    dataframe['adx'] = ta.ADX(dataframe)
    stoch = ta.STOCHF(dataframe)
    dataframe['fastd'] = stoch['fastd']
    dataframe['fastk'] = stoch['fastk']
    dataframe['blower'] = ta.BBANDS(dataframe, nbdevup=2, nbdevdn=2)['lowerband']
    dataframe['sma'] = ta.SMA(dataframe, timeperiod=40)
    dataframe['tema'] = ta.TEMA(dataframe, timeperiod=9)
    dataframe['mfi'] = ta.MFI(dataframe)
    dataframe['rsi'] = ta.RSI(dataframe)
    dataframe['ema5'] = ta.EMA(dataframe, timeperiod=5)
    dataframe['ema10'] = ta.EMA(dataframe, timeperiod=10)
    dataframe['ema50'] = ta.EMA(dataframe, timeperiod=50)
    dataframe['ema100'] = ta.EMA(dataframe, timeperiod=100)
    dataframe['ao'] = awesome_oscillator(dataframe)
    macd = ta.MACD(dataframe)
    dataframe['macd'] = macd['macd']
    dataframe['macdsignal'] = macd['macdsignal']
    dataframe['macdhist'] = macd['macdhist']
    hilbert = ta.HT_SINE(dataframe)
    dataframe['htsine'] = hilbert['sine']
    dataframe['htleadsine'] = hilbert['leadsine']
    dataframe['plus_dm'] = ta.PLUS_DM(dataframe)
    dataframe['plus_di'] = ta.PLUS_DI(dataframe)
    dataframe['minus_dm'] = ta.MINUS_DM(dataframe)
    dataframe['minus_di'] = ta.MINUS_DI(dataframe)
    return dataframe
```

## Bibliotecas de indicadores:

- **ta-lib**
- **pandas-ta**
- **técnico**

Bibliotecas técnicas adicionais podem ser instaladas conforme necessário, ou indicadores personalizados podem ser escritos / inventados pelo autor da estratégia.

### Período de inicialização da estratégia:

A maioria dos indicadores tem um período de inicialização instável, no qual eles não estão disponíveis ou o cálculo está incorreto. Isso pode levar a inconsistências, uma vez que a Freqtrade não sabe qual deve ser a duração desse período de instabilidade. Para dar conta disso, a estratégia pode receber o `startup_candle_count` atributo. Isso deve ser definido como o número máximo de velas que a estratégia requer para calcular indicadores estáveis.

Nesta estratégia de exemplo, isso deve ser definido como 100 ( startup_candle_count = 100), uma vez que o histórico mais longo necessário é de 100 velas.

`    dataframe['ema100'] = ta.EMA(dataframe, timeperiod=100)`

Ao permitir que o bot saiba quanto histórico é necessário, as negociações de backtest podem começar no intervalo de tempo especificado durante o backtesting e hiperopt.

## Regras de compra de sinais:

Edite o método `populate_buy_trend()` em seu arquivo de estratégia para atualizar sua estratégia de compra.

É importante sempre retornar o dataframe sem remover / modificar as colunas ` "open", "high", "low", "close", "volume"`, caso contrário, esses campos conteriam algo inesperado.

Este método também definirá uma nova coluna, "buy" que deve conter 1 para compras e 0 para "nenhuma ação".

Amostra de user_data/strategies/sample_strategy.py:


```
def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
    """
    Based on TA indicators, populates the buy signal for the given dataframe
    :param dataframe: DataFrame populated with indicators
    :param metadata: Additional information, like the currently traded pair
    :return: DataFrame with buy column
    """
    dataframe.loc[
        (
            (qtpylib.crossed_above(dataframe['rsi'], 30)) &  # Signal: RSI crosses above 30
            (dataframe['tema'] <= dataframe['bb_middleband']) &  # Guard
            (dataframe['tema'] > dataframe['tema'].shift(1)) &  # Guard
            (dataframe['volume'] > 0)  # Make sure Volume is not 0
        ),
        'buy'] = 1

    return dataframe
```

**Observação !!!**

Comprar exige que os vendedores comprem - portanto, o volume precisa ser> 0 ( dataframe['volume'] > 0) para garantir que o bot não compre / venda em períodos de inatividade.

## Regras de sinal de venda:
Edite o método populate_sell_trend()em seu arquivo de estratégia para atualizar sua estratégia de venda. Observe que o sinal de venda só é usado se use_sell_signalfor definido como verdadeiro na configuração.

É importante sempre retornar o dataframe sem remover / modificar as colunas `"open", "high", "low", "close", "volume"`, caso contrário, esses campos conteriam algo inesperado.

Este método também definirá uma nova coluna, "sell"que deve conter 1 para vendas e 0 para "nenhuma ação".

Amostra de user_data/strategies/sample_strategy.py:

```
def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
    """
    Based on TA indicators, populates the sell signal for the given dataframe
    :param dataframe: DataFrame populated with indicators
    :param metadata: Additional information, like the currently traded pair
    :return: DataFrame with buy column
    """
    dataframe.loc[
        (
            (qtpylib.crossed_above(dataframe['rsi'], 70)) &  # Signal: RSI crosses above 70
            (dataframe['tema'] > dataframe['bb_middleband']) &  # Guard
            (dataframe['tema'] < dataframe['tema'].shift(1)) &  # Guard
            (dataframe['volume'] > 0)  # Make sure Volume is not 0
        ),
        'sell'] = 1
    return dataframe
```

## ROI mínimo:

Este dict define o Return On Investment (ROI) mínimo que uma negociação deve alcançar antes de vender, independente do sinal de venda.

É do seguinte formato, com a tecla dict (lado esquerdo dos dois pontos) sendo os minutos passados ​​desde a abertura da negociação, e o valor (lado direito dos dois pontos) sendo a porcentagem.

```
minimal_roi = {
    "40": 0.0,
    "30": 0.01,
    "20": 0.02,
    "0": 0.04
}
```

A configuração acima significaria, portanto:

- Venda sempre que o lucro de 4% for alcançado
- Venda quando o lucro de 2% for alcançado (em vigor após 20 minutos)
- Venda quando o lucro de 1% for alcançado (em vigor após 30 minutos)
- Venda quando a negociação não estiver perdendo (em vigor após 40 minutos)


# Trailing stop loss, perda positiva personalizada¶

Também é possível ter um stop loss padrão, quando você está no vermelho com sua compra (compra - taxa), mas assim que você atingir o resultado positivo, o sistema utilizará um novo stop loss, que pode ter um valor diferente. Por exemplo, seu stop loss padrão é -10%, mas quando você tiver mais de 0% de lucro (exemplo 0,1%), um stoploss diferente será usado.

### Observação

Se você deseja que o stoploss seja alterado apenas quando você atingir o ponto de equilíbrio de obter lucro (o que a maioria dos usuários deseja), consulte a próxima seção com o deslocamento habilitado .

Ambos os valores precisam trailing_stopser definidos como verdadeiros e trailing_stop_positivecom um valor.

```
    stoploss = -0.10
    trailing_stop = True
    trailing_stop_positive = 0.02
```    
### Por exemplo, matemática simplificada:

- o bot compra um ativo a um preço de $ 100
- o stop loss é definido em -10%
- o stop loss seria acionado quando o ativo caísse abaixo de 90 $
- assumindo que o ativo agora aumenta para 102 $
- o stop loss será de -2% de 102 $ = 99,96 $ (o stop loss de 99,96 $ será bloqueado e seguirá os incrementos de preço do ativo com -2%)
- agora o valor do ativo cai para 101 $, o stop loss ainda será 99,96 $ e desencadearia em 99,96 $
- O 0,02 se traduziria em uma perda de parada de -2%. Antes disso, stoplossé usado para o stoploss à direita.

***

Trailing stop loss apenas quando o comércio atingiu um certo deslocamento¶
Também é possível usar um `stoploss` estático até que o deslocamento seja alcançado e, em seguida, rastrear a negociação para obter lucros quando o mercado virar.

`"trailing_only_offset_is_reached": true` Nesse caso , o `stoploss` posterior é ativado apenas quando o deslocamento é alcançado. Até então, o stoploss permanece no configurado `stoploss`. Esta opção pode ser usada com ou sem `trailing_stop_positive`, mas usa ## `trailing_stop_positive_offset` como deslocamento.

```
    trailing_stop_positive_offset = 0.011
    trailing_only_offset_is_reached = True
```    
### Configuração (o deslocamento é o preço de compra + 3%):

```
    stoploss = -0.10
    trailing_stop = True
    trailing_stop_positive = 0.02
    trailing_stop_positive_offset = 0.03
    trailing_only_offset_is_reached = True
```
### Por exemplo, matemática simplificada:

- o bot compra um ativo a um preço de $ 100
- o stop loss é definido em -10%
- o stop loss seria acionado quando o ativo caísse abaixo de 90 $
- o stoploss permanecerá em 90 $, a menos que o ativo aumente para ou acima do deslocamento configurado
- assumindo que o ativo agora aumenta para 103 $ (onde temos o deslocamento configurado)
- o stop loss será agora -2% de 103 $ = 100,94 $
- agora o valor do ativo cai para 101 $, o stop loss ainda será 100,94 $ e acionaria 100,94 $
Gorjeta

Certifique-se de ter este valor ( trailing_stop_positive_offset) inferior ao ROI mínimo, caso contrário, o ROI mínimo será aplicado primeiro e venderá a negociação.

