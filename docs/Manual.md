# Configure o bot

Freqtrade tem muitos recursos e possibilidades configuráveis. Por padrão, essas configurações são definidas por meio do arquivo de configuração (veja abaixo).

## O arquivo de configuração Freqtrade

O bot usa um conjunto de parâmetros de configuração durante sua operação que todos juntos estão de acordo com a configuração do bot. Normalmente lê sua configuração a partir de um arquivo (arquivo de configuração Freqtrade).

Por padrão, o bot carrega a configuração do arquivo config.json, localizado no diretório de trabalho userdata.

Você pode especificar um arquivo de configuração diferente usado pelo bot com a `-c/--config`  opção de linha de comando.

Se você usou o método de início rápido para instalar o bot, o script de instalação já deve ter criado o arquivo de configuração padrão ( `config.json`) para você.

Se o arquivo de configuração padrão não for criado, recomendamos usá-lo `freqtrade new-config --config config.json` para gerar um arquivo de configuração básico.

O arquivo de configuração Freqtrade deve ser escrito no formato JSON.

Além da sintaxe JSON padrão, você pode usar comentários de uma `// ...ou várias linhas /* ... */em seus arquivos de configuração e vírgulas finais nas listas de parâmetros.


Não se preocupe se você não estiver familiarizado com o formato JSON - basta abrir o arquivo de configuração com um editor de sua escolha, fazer algumas alterações nos parâmetros de que precisa, salvar suas alterações e, por fim, reinicie o bot ou, se era anteriormente parado, execute-o novamente com as alterações feitas na configuração. O bot valida a sintaxe do arquivo de configuração na inicialização e avisa se você cometeu algum erro ao editá-lo, apontando linhas problemáticas.
***
## Variáveis ​​ambientais

Defina as opções na configuração do Freqtrade por meio de variáveis ​​de ambiente. **Isso tem prioridade sobre o valor correspondente na configuração ou estratégia.**

As variáveis ​​de ambiente devem ser prefixadas com FREQTRADE__para serem carregadas na configuração do freqtrade.

`__` serve como separador de nível, portanto, o formato usado deve corresponder a `FREQTRADE__{section}__{key}`. Como tal - uma variável de ambiente definida como `export FREQTRADE__STAKE_AMOUNT=200`resultaria em `{stake_amount: 200}`.

Um exemplo mais complexo pode ser `export FREQTRADE__EXCHANGE__KEY=<yourExchangeKey>` manter sua chave de troca em segredo. Isso moverá o valor para a `exchange.key` seção da configuração. Usando este esquema, todas as definições de configuração também estarão disponíveis como variáveis ​​de ambiente.

Observe que as variáveis ​​de ambiente sobrescreverão as configurações correspondentes em sua configuração, mas os argumentos da linha de comando sempre vencerão. 
 ## Observação: 
 As variáveis ​​de ambiente detectadas são registradas na inicialização -   portanto, se você não conseguir descobrir por que um valor não é o que   você acha que deveria ser com base na configuração, certifique-se de que    não foi carregado a partir de uma variável de ambiente.



## Parâmetros de configuração:

A tabela a seguir listará todos os parâmetros de configuração disponíveis.

Freqtrade também pode carregar muitas opções por meio de argumentos de linha de comando (CLI) (verifique a saída dos comando `--help` para obter detalhes).  A prevalência para todas as opções é a seguinte:

- Os argumentos CLI substituem qualquer outra opção
- [Variáveis ​​ambientais](https://www.freqtrade.io/en/stable/configuration/#environment-variables)
- Os arquivos de configuração são usados ​​em sequência (o último arquivo vence) e substituem as configurações de Estratégia.
- As configurações de estratégia são usadas apenas se não forem definidas por meio de argumentos de configuração ou linha de comando. Essas opções são marcadas com [Substituição de Estratégia](https://www.freqtrade.io/en/stable/configuration/#parameters-in-the-strategy) na tabela abaixo.

Os parâmetros obrigatórios são marcados como Obrigatórios , o que significa que devem ser configurados de uma das maneiras possíveis.

| Parâmetro                                             	| Descrição                                                                                                                                                                                                                                                                                                                                                                                	|
|-------------------------------------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| max_open_trades                                       	| Obrigatório. Número de negociações abertas que seu bot pode ter. Apenas uma negociação aberta por par é possível, então o comprimento de sua pairlist é outra limitação que pode ser aplicada. Se -1, então ele é ignorado (ou seja, negociações abertas potencialmente ilimitadas, limitadas pelo pairlist). Mais informações abaixo .<br>Tipo de dados: Número inteiro positivo ou -1. 	|
| stake_currency                                        	| Obrigatório. Moeda criptográfica usada para negociação.<br>Tipo de dados: String                                                                                                                                                                                                                                                                                                         	|
| stake_amount                                          	| Obrigatório. Quantidade de criptomoeda que seu bot usará para cada negociação. Defina-o como "unlimited"para permitir que o bot use todo o saldo disponível. Mais informações abaixo .<br>Tipo de dados: flutuante positivo ou "unlimited".                                                                                                                                              	|
| tradable_balance_ratio                                	| Proporção do saldo total da conta que o bot está autorizado a negociar. Mais informações abaixo .<br>O padrão é 0.9999%).<br>Tipo de dados: flutuação positiva entre 0.1e 1.0.                                                                                                                                                                                                           	|
| available_capital                                     	| Capital inicial disponível para o bot. Útil ao executar vários bots na mesma conta de troca. Mais informações abaixo .<br>Tipo de dados: flutuante positivo.                                                                                                                                                                                                                             	|
| amend_last_stake_amount                               	| Use o valor reduzido da última aposta, se necessário. Mais informações abaixo .<br>O padrão é false.<br>Tipo de dados: booleano                                                                                                                                                                                                                                                          	|
| last_stake_amount_min_ratio                           	| Define o valor mínimo da aposta que deve ser deixada e executada. Aplica-se apenas ao último valor da aposta quando é alterado para um valor reduzido (ou seja, se amend_last_stake_amountfor definido como true). Mais informações abaixo .<br>O padrão é 0.5.<br>Tipo de dados: Flutuante (como proporção)                                                                             	|
| amount_reserve_percent                                	| Reserve alguma quantia no valor mínimo da aposta do par. O bot irá reservar amount_reserve_percent+ valor de stoploss ao calcular o valor mínimo do par de aposta, a fim de evitar possíveis recusas de negociação.<br>O padrão é 0.05(5%).<br>Tipo de dados: Float positivo como proporção.                                                                                             	|
| timeframe                                             	| O prazo (ex intervalo ticker) para usar (por exemplo 1m, 5m, 15m, 30m, 1h...). Substituição de estratégia .<br>Tipo de dados: String                                                                                                                                                                                                                                                     	|
| fiat_display_currency                                 	| Moeda Fiat usada para mostrar seus lucros. Mais informações abaixo .<br>Tipo de dados: String                                                                                                                                                                                                                                                                                            	|
| dry_run                                               	| Obrigatório. Defina se o bot deve estar em Dry Run ou modo de produção.<br>O padrão é true.<br>Tipo de dados: booleano                                                                                                                                                                                                                                                                   	|
| dry_run_wallet                                        	| Defina o valor inicial na moeda da aposta para a carteira simulada usada pelo bot em execução no modo Dry Run.<br>O padrão é 1000.<br>Tipo de dados: Float                                                                                                                                                                                                                               	|
| cancel_open_orders_on_exit                            	| Cancele os pedidos abertos quando o /stopcomando RPC for emitido, Ctrl+Cpressionado ou o bot morrer inesperadamente. Quando definido como true, isso permite que você /stopcancele pedidos não preenchidos e parcialmente preenchidos no caso de um crash do mercado. Não afeta as posições abertas.<br>O padrão é false.<br>Tipo de dados: booleano                                     	|
| process_only_new_candles                              	| Habilite o processamento de indicadores apenas quando novas velas chegarem. Se false, cada loop preenche os indicadores, isso significa que o mesmo candle é processado muitas vezes, criando a carga do sistema, mas pode ser útil porque sua estratégia depende dos dados do tick, não apenas do candle. Substituição de estratégia .<br>O padrão é false.<br>Tipo de dados: booleano  	|
| minimal_roi                                           	| Obrigatório. Defina o limite como a proporção que o bot usará para vender uma negociação. Mais informações abaixo . Substituição de estratégia .<br>Tipo de dados: Dict                                                                                                                                                                                                                  	|
| stoploss                                              	| Obrigatório. Valor como proporção do stoploss usado pelo bot. Mais detalhes na documentação do stoploss . Substituição de estratégia .<br>Tipo de dados: Flutuante (como proporção)                                                                                                                                                                                                      	|
| trailing_stop                                         	| Habilita o stoploss à direita (com base stoplossna configuração ou no arquivo de estratégia). Mais detalhes na documentação do stoploss . Substituição de estratégia .<br>Tipo de dados: booleano                                                                                                                                                                                        	|
| trailing_stop_positive                                	| Muda o stoploss assim que o lucro for alcançado. Mais detalhes na documentação do stoploss . Substituição de estratégia .<br>Tipo de dados: Float                                                                                                                                                                                                                                        	|
| trailing_stop_positive_offset                         	| Compensar quando aplicar trailing_stop_positive. Valor percentual que deve ser positivo. Mais detalhes na documentação do stoploss . Substituição de estratégia .<br>O padrão é 0.0(sem deslocamento).<br>Tipo de dados: Float                                                                                                                                                           	|
| trailing_only_offset_is_reached                       	| Aplique stoploss à direita apenas quando o deslocamento for alcançado. documentação de stoploss . Substituição de estratégia .<br>O padrão é false.<br>Tipo de dados: booleano                                                                                                                                                                                                           	|
| fee                                                   	| Taxa usada durante o backtesting / ensaios. Normalmente não deve ser configurado, o que faz com que o freqtrade volte para a taxa padrão do Exchange. Defina como proporção (por exemplo, 0,001 = 0,1%). A taxa é aplicada duas vezes para cada negociação, uma na compra e outra na venda.<br>Tipo de dados: Flutuante (como proporção)                                                 	|
| unfilledtimeout.buy                                   	| Obrigatório. Quanto tempo (em minutos ou segundos) o bot irá esperar pela conclusão de uma ordem de compra não preenchida, após o qual a ordem será cancelada e repetida ao preço atual (novo), enquanto houver um sinal. Substituição de estratégia .<br>Tipo de dados: Inteiro                                                                                                         	|
| unfilledtimeout.sell                                  	| Obrigatório. Quanto tempo (em minutos ou segundos) o bot irá esperar pela conclusão de uma ordem de venda não preenchida, após o qual a ordem será cancelada e repetida ao preço atual (novo), enquanto houver um sinal. Substituição de estratégia .<br>Tipo de dados: Inteiro                                                                                                          	|
| unfilledtimeout.unit                                  	| Unidade a ser usada na configuração de tempo limite não preenchido. Observação: se você definir unfilledtimeout.unit como "segundos", "internals.process_throttle_secs" deve ser inferior ou igual ao tempo limite de substituição de estratégia .<br>O padrão é minutes.<br>Tipo de dados: String                                                                                       	|
| bid_strategy.price_side                               	| Selecione o lado do spread que o bot deve olhar para obter a taxa de compra. Mais informações abaixo .<br>O padrão é bid.<br>Tipo de dados: String ( askou bid).                                                                                                                                                                                                                         	|
| bid_strategy.ask_last_balance                         	| Obrigatório. Interpole o preço de licitação. Mais informações abaixo .                                                                                                                                                                                                                                                                                                                   	|
| bid_strategy.use_order_book                           	| Habilite a compra usando as taxas dos lances do livro de pedidos .<br>Tipo de dados: booleano                                                                                                                                                                                                                                                                                            	|
| bid_strategy.order_book_top                           	| O bot usará a taxa N superior no livro de pedidos "price_side" para comprar. Ou seja, um valor de 2 permitirá que o bot para escolher o 2 nd taxa de compra em Licitações encomendar o livro .<br>O padrão é 1.<br>Tipo de dados: Número inteiro positivo                                                                                                                                	|
| bid_strategy. check_depth_of_market.enabled           	| Não compre se a diferença entre as ordens de compra e as ordens de venda for atendida no Livro de Ofertas. Verifique a profundidade do mercado .<br>O padrão é false.<br>Tipo de dados: booleano                                                                                                                                                                                         	|
| bid_strategy. check_depth_of_market.bids_to_ask_delta 	| A relação de diferença entre as ordens de compra e as ordens de venda encontradas no Livro de Ofertas. Um valor abaixo de 1 significa que o tamanho do pedido de venda é maior, enquanto um valor maior que 1 significa que o tamanho do pedido de compra é maior. Verifique a profundidade do mercado .<br>Padrões para 0.<br>Tipo de dados: Flutuante (como proporção)                 	|
| ask_strategy.price_side                               	| Selecione o lado do spread que o bot deve olhar para obter a taxa de venda. Mais informações abaixo .<br>O padrão é ask.<br>Tipo de dados: String ( askou bid).                                                                                                                                                                                                                          	|
| ask_strategy.bid_last_balance                         	| Interpole o preço de venda. Mais informações abaixo .                                                                                                                                                                                                                                                                                                                                    	|
| ask_strategy.use_order_book                           	| Habilite a venda de negociações abertas usando pedidos do livro de pedidos .<br>Tipo de dados: booleano                                                                                                                                                                                                                                                                                  	|
| ask_strategy.order_book_top                           	| O bot usará a taxa N superior no livro de pedidos "price_side" para vender. Ou seja, um valor de 2 permitirá que o bot para escolher o 2 nd pedir taxa em Transações Solicita<br>Padrões para 1.<br>Tipo de dados: Número inteiro positivo                                                                                                                                               	|
| use_sell_signal                                       	| Use sinais de venda produzidos pela estratégia além do minimal_roi. Substituição de estratégia .<br>O padrão é true.<br>Tipo de dados: booleano                                                                                                                                                                                                                                          	|
| sell_profit_only                                      	| Espere até que o bot chegue sell_profit_offsetantes de tomar uma decisão de venda. Substituição de estratégia .<br>O padrão é false.<br>Tipo de dados: booleano                                                                                                                                                                                                                          	|
| sell_profit_offset                                    	| O sinal de venda só está ativo acima deste valor. Ativo apenas em combinação com sell_profit_only=True. Substituição de estratégia .<br>O padrão é 0.0.<br>Tipo de dados: Flutuante (como proporção)                                                                                                                                                                                     	|
| ignore_roi_if_buy_signal                              	| Não venda se o sinal de compra ainda estiver ativo. Esta configuração tem preferência sobre minimal_roie use_sell_signal. Substituição de estratégia .<br>O padrão é false.<br>Tipo de dados: booleano                                                                                                                                                                                   	|
| ignore_buying_expired_candle_after                    	| Especifica o número de segundos até que um sinal de compra não seja mais usado.<br>Tipo de dados: Inteiro                                                                                                                                                                                                                                                                                	|
| order_types                                           	| Order-tipos Configurar dependendo da acção ( "buy", "sell", "stoploss", "stoploss_on_exchange"). Mais informações abaixo . Substituição de estratégia .<br>Tipo de dados: Dict                                                                                                                                                                                                           	|
| order_time_in_force                                   	| Configure o tempo em vigor para ordens de compra e venda. Mais informações abaixo . Substituição de estratégia .<br>Tipo de dados: Dict                                                                                                                                                                                                                                                  	|
| custom_price_max_distance_ratio                       	| Configure a relação de distância máxima entre o preço de entrada ou saída atual e personalizado.<br>O padrão é 0.022%).<br>Tipo de dados: flutuação positiva                                                                                                                                                                                                                             	|
| exchange.name                                         	| Obrigatório. Nome da classe de intercâmbio a ser usada. Lista abaixo .<br>Tipo de dados: String                                                                                                                                                                                                                                                                                          	|
| exchange.sandbox                                      	| Use a versão 'sandbox' da troca, onde a troca fornece uma caixa de areia para integração sem riscos. Veja aqui em mais detalhes.<br>Tipo de dados: booleano                                                                                                                                                                                                                              	|
| exchange.key                                          	| Chave API a ser usada para a troca. Requerido apenas quando você está no modo de produção.<br>Mantenha isso em segredo, não divulgue publicamente.<br>Tipo de dados: String                                                                                                                                                                                                              	|
| exchange.secret                                       	| Segredo da API a ser usado para a troca. Requerido apenas quando você está no modo de produção.<br>Mantenha isso em segredo, não divulgue publicamente.<br>Tipo de dados: String                                                                                                                                                                                                         	|
| exchange.password                                     	| Senha da API a ser usada para a troca. Requerido apenas quando você está no modo de produção e para trocas que usam senha para solicitações de API.<br>Mantenha isso em segredo, não divulgue publicamente.<br>Tipo de dados: String                                                                                                                                                     	|
| exchange.pair_whitelist                               	| Lista de pares a serem usados pelo bot para negociação e para verificar possíveis negociações durante o backtesting. Suporta pares de regex como .*/BTC. Não usado por VolumePairList. Mais informações .<br>Tipo de dados: Lista                                                                                                                                                        	|
| exchange.pair_blacklist                               	| Lista de pares que o bot deve absolutamente evitar para negociação e backtesting. Mais informações .<br>Tipo de dados: Lista                                                                                                                                                                                                                                                             	|
| exchange.ccxt_config                                  	| Parâmetros CCXT adicionais passados para ambas as instâncias ccxt (sincronização e assíncrona). Normalmente, este é o local correto para configurações de ccxt. Os parâmetros podem diferir de troca para troca e estão documentados na documentação do ccxt.<br>Tipo de dados: Dict                                                                                                     	|
| exchange.ccxt_sync_config                             	| Parâmetros CCXT adicionais passados para a instância ccxt regular (sincronização). Os parâmetros podem diferir de troca para troca e estão documentados na documentação do ccxt.<br>Tipo de dados: Dict                                                                                                                                                                                  	|
| exchange.ccxt_async_config                            	| Parâmetros CCXT adicionais passados para a instância ccxt assíncrona. Os parâmetros podem diferir de troca para troca e estão documentados na documentação do ccxt.<br>Tipo de dados: Dict                                                                                                                                                                                               	|
| exchange.markets_refresh_interval                     	| O intervalo em minutos em que os mercados são recarregados.<br>O padrão é 60minutos.<br>Tipo de dados: Número inteiro positivo                                                                                                                                                                                                                                                           	|
| exchange.skip_pair_validation                         	| Pule a validação do pairlist na inicialização.<br>O padrão é o tipo de false<br>dados: * Booleano                                                                                                                                                                                                                                                                                        	|
| exchange.skip_open_order_update                       	| Ignora atualizações de pedidos abertos na inicialização, caso a troca cause problemas. Relevante apenas em condições ao vivo.<br>O padrão é o tipo de false<br>dados: * Booleano                                                                                                                                                                                                         	|
| exchange.log_responses                                	| Registre as respostas de troca relevantes. Apenas para modo de depuração - use com cuidado.<br>O padrão é o tipo de false<br>dados: * Booleano                                                                                                                                                                                                                                           	|
| edge.*                                                	| Consulte o documento de configuração de borda para obter uma explicação detalhada.                                                                                                                                                                                                                                                                                                       	|
| experimental.block_bad_exchanges                      	| Trocas de bloco que não funcionam com freqtrade. Deixe o padrão a menos que você queira testar se essa troca funciona agora.<br>O padrão é true.<br>Tipo de dados: booleano                                                                                                                                                                                                              	|
| pairlists                                             	| Defina um ou mais pairlists a serem usados. Mais informações .<br>O padrão é StaticPairList.<br>Tipo de dados: lista de dictos                                                                                                                                                                                                                                                           	|
| protections                                           	| Defina uma ou mais proteções a serem usadas. Mais informações .<br>Tipo de dados: lista de dictos                                                                                                                                                                                                                                                                                        	|
| telegram.enabled                                      	| Habilite o uso do Telegram.<br>Tipo de dados: booleano                                                                                                                                                                                                                                                                                                                                   	|
| telegram.token                                        	| Seu token de bot do Telegram. Requerido apenas se telegram.enabledfor true.<br>Mantenha isso em segredo, não divulgue publicamente.<br>Tipo de dados: String                                                                                                                                                                                                                             	|
| telegram.chat_id                                      	| O ID da sua conta pessoal do Telegram. Requerido apenas se telegram.enabledfor true.<br>Mantenha isso em segredo, não divulgue publicamente.<br>Tipo de dados: String                                                                                                                                                                                                                    	|
| telegram.balance_dust_level                           	| Nível de poeira (na moeda da aposta) - moedas com saldo abaixo deste não serão mostradas por /balance.<br>Tipo de dados: float                                                                                                                                                                                                                                                           	|
| webhook.enabled                                       	| Habilitar o uso de notificações de Webhook<br>Tipo de dados: Booleano                                                                                                                                                                                                                                                                                                                    	|
| webhook.url                                           	| URL para o webhook. Requerido apenas se webhook.enabledfor true. Consulte a documentação do webhook para obter mais detalhes.<br>Tipo de dados: String                                                                                                                                                                                                                                   	|
| webhook.webhookbuy                                    	| Carga útil para enviar na compra. Requerido apenas se webhook.enabledfor true. Consulte a documentação do webhook para obter mais detalhes.<br>Tipo de dados: String                                                                                                                                                                                                                     	|
| webhook.webhookbuycancel                              	| Carga útil para enviar no cancelamento da ordem de compra. Requerido apenas se webhook.enabledfor true. Consulte a documentação do webhook para obter mais detalhes.<br>Tipo de dados: String                                                                                                                                                                                            	|
| webhook.webhooksell                                   	| Carga útil para enviar para venda. Requerido apenas se webhook.enabledfor true. Consulte a documentação do webhook para obter mais detalhes.<br>Tipo de dados: String                                                                                                                                                                                                                    	|
| webhook.webhooksellcancel                             	| Carga útil para enviar no cancelamento da ordem de venda. Requerido apenas se webhook.enabledfor true. Consulte a documentação do webhook para obter mais detalhes.<br>Tipo de dados: String                                                                                                                                                                                             	|
| webhook.webhookstatus                                 	| Carga útil para enviar chamadas de status. Requerido apenas se webhook.enabledfor true. Consulte a documentação do webhook para obter mais detalhes.<br>Tipo de dados: String                                                                                                                                                                                                            	|
| api_server.enabled                                    	| Habilite o uso do servidor API. Consulte a documentação do servidor API para obter mais detalhes.<br>Tipo de dados: booleano                                                                                                                                                                                                                                                             	|
| api_server.listen_ip_address                          	| Endereço IP de ligação. Consulte a documentação do servidor API para obter mais detalhes.<br>Tipo de dados: IPv4                                                                                                                                                                                                                                                                         	|
| api_server.listen_port                                	| Bind Port. Consulte a documentação do servidor API para obter mais detalhes.<br>Tipo de dados: número inteiro entre 1024 e 65535                                                                                                                                                                                                                                                         	|
| api_server.verbosity                                  	| Detalhamento de registro. infoimprimirá todas as chamadas RPC, enquanto "erro" exibirá apenas os erros.<br>Tipo de dados: Enum, infoou error. O padrão é info.                                                                                                                                                                                                                           	|
| api_server.username                                   	| Nome de usuário do servidor API. Consulte a documentação do servidor API para obter mais detalhes.<br>Mantenha isso em segredo, não divulgue publicamente.<br>Tipo de dados: String                                                                                                                                                                                                      	|
| api_server.password                                   	| Senha para servidor API. Consulte a documentação do servidor API para obter mais detalhes.<br>Mantenha isso em segredo, não divulgue publicamente.<br>Tipo de dados: String                                                                                                                                                                                                              	|
| bot_name                                              	| Nome do bot. Passado via API para um cliente - pode ser mostrado para distinguir / nomear bots.<br>O padrão é o tipo de freqtrade<br>dados: String                                                                                                                                                                                                                                       	|
| db_url                                                	| Declara o URL do banco de dados a ser usado. NOTA: O padrão é sqlite:///tradesv3.dryrun.sqliteif e to para instâncias de produção. Tipo de dados: String, string de conexão SQLAlchemydry_runtruesqlite:///tradesv3.sqlite                                                                                                                                                               	|
| initial_state                                         	| Define o estado inicial do aplicativo. Se definido como interrompido, o bot deve ser iniciado explicitamente por meio do /startcomando RPC.<br>O padrão é stopped.<br>Tipo de dados: Enum, stoppedourunning                                                                                                                                                                              	|
| forcebuy_enable                                       	| Habilita os Comandos RPC para forçar uma compra. Mais informações abaixo.<br>Tipo de dados: booleano                                                                                                                                                                                                                                                                                     	|
| disable_dataframe_checks                              	| Desative a verificação do dataframe OHLCV retornado dos métodos de estratégia para correção. Use apenas ao alterar intencionalmente o dataframe e entenda o que você está fazendo. Substituição de estratégia .<br>O padrão éFalse .<br>Tipo de dados: booleano                                                                                                                          	|
| strategy                                              	| Requerido Define a classe de estratégia a ser usada. Recomendado para ser definido via --strategy NAME.<br>Tipo de dados: ClassName                                                                                                                                                                                                                                                      	|
| strategy_path                                         	| Adiciona um caminho de pesquisa de estratégia adicional (deve ser um diretório).<br>Tipo de dados: String                                                                                                                                                                                                                                                                                	|
| internals.process_throttle_secs                       	| Defina a aceleração do processo ou a duração mínima do loop para um loop de iteração do bot. Valor em segundo.<br>O padrão é 5segundos.<br>Tipo de dados: Número inteiro positivo                                                                                                                                                                                                        	|
| internals.heartbeat_interval                          	| Imprimir mensagem de pulsação a cada N segundos. Defina como 0 para desativar as mensagens de pulsação.<br>O padrão é 60segundos.<br>Tipo de dados: Número inteiro positivo ou 0                                                                                                                                                                                                         	|
| internals.sd_notify                                   	| Habilita o uso do protocolo sd_notify para informar ao gerente de serviço systemd sobre mudanças no estado do bot e emitir pings keep-alive. Veja aqui para mais detalhes.<br>Tipo de dados: booleano                                                                                                                                                                                    	|
| logfile                                               	| Especifica o nome do arquivo de log. Usa uma estratégia contínua para rotação de arquivo de log para 10 arquivos com o limite de 1 MB por arquivo.<br>Tipo de dados: String                                                                                                                                                                                                              	|
| user_data_dir                                         	| Diretório contendo dados do usuário.<br>O padrão é./user_data/ .<br>Tipo de dados: String                                                                                                                                                                                                                                                                                                	|
| dataformat_ohlcv                                      	| Formato de dados a ser usado para armazenar dados históricos de velas (OHLCV).<br>O padrão éjson .<br>Tipo de dados: String                                                                                                                                                                                                                                                              	|
| dataformat_trades                                     	| Formato de dados a ser usado para armazenar dados históricos de negociações.<br>O padrão éjsongz .<br>Tipo de dados: String                                                                                                                                                                                                                                                              	|


***
## Parâmetros na estratégia:

Os parâmetros a seguir podem ser definidos no arquivo de configuração ou estratégia. Os valores definidos no arquivo de configuração sempre substituem os valores definidos na estratégia.

- **minimal_roi**
- **timeframe**
- **stoploss**
- **trailing_stop**
- **trailing_stop_positive**
- **trailing_stop_positive_offset**
- **trailing_only_offset_is_reached**
- **use_custom_stoploss**
- **process_only_new_candles**
- **order_types**
- **order_time_in_force**
- **unfilledtimeout**
- **disable_dataframe_checks**
- **use_sell_signal**
- **sell_profit_only**
- **sell_profit_offset**
- **ignore_roi_if_buy_signal**
- **ignore_buying_expired_candle_after**

## Configurando o valor por negociação:

Existem vários métodos para configurar quanto da moeda da aposta o bot usará para entrar em uma negociação. Todos os métodos respeitam a configuração de saldo disponível conforme explicado a seguir.

**Aposta mínima de negociação**

O valor mínimo da aposta dependerá da troca e do par e geralmente está listado nas páginas de suporte de transação da exchange . Assumindo que o valor mínimo negociável para XRP/USD é 20 XRP (dado pela transação) e o preço é $0,6.

O valor mínimo da aposta para comprar este par é, portanto `20 * 0.6 ~= 12`. Essa troca também tem um limite de USD - onde todos os pedidos devem ser >10 $ - que, entretanto, não se aplica neste caso.

Para garantir uma execução segura, o freqtrade não permitirá a compra com um valor de aposta de 10,1$, em vez disso, ele se certificará de que há espaço suficiente para colocar um stoploss abaixo do par (+ um deslocamento, definido por `amount_reserve_percent`, que é padronizado em 5% )


Com uma reserva de 5%, o valor mínimo da aposta seria de  `12,6 $ ( 12 * (1 + 0.05))`. Se levarmos em consideração um stoploss de 10% além disso - terminaríamos com um valor de ~ `14 $ (12.6/(1-0.1))`.

Para limitar este cálculo no caso de grandes valores de stoploss, o limite mínimo de aposta calculado nunca será mais do que 50% acima do limite real.

## Aviso!!!
Como os limites das transaçoes são geralmente estáveis ​​e não são atualizados com frequência, alguns pares podem apresentar limites mínimos bem altos, simplesmente porque o preço aumentou muito desde o último ajuste de limite pela troca.


***
## Saldo negociável

Por padrão, o bot assume que o `complete amount - 1%` está à sua disposição e, ao usar o `dynamic stake amount` , ele dividirá o saldo completo em `max_open_trades` blocos por negociação. O Freqtrade reservará 1% para eventuais taxas ao entrar em uma negociação e portanto não tocará nisso por padrão.

Você pode configurar a quantidade "intocada" usando a `tradable_balance_ratio` configuração.

Por exemplo, se você tiver 10ETH disponíveis em sua carteira na bolsa e `tradable_balance_ratio=0.5` (que é 50%), o bot usará um valor máximo de 5 ETH para negociação e considera isso como um saldo disponível. O resto da carteira não será afetado pelas transações.
***

## Perigo!!!

Esta configuração não deve ser usada ao executar vários bots na mesma conta. Em vez disso, olhe para [Capital disponível](https://www.freqtrade.io/en/stable/configuration/#assign-available-capital) para o bot .

***
## Aviso !!!
A `tradable_balance_ratio` configuração se aplica ao saldo atual **(saldo livre + empatado em negociações)** . Portanto, assumindo o saldo inicial de 1000, uma configuração com `tradable_balance_ratio=0.99` não garantirá que 10 unidades monetárias sempre permanecerão disponíveis na troca. Por exemplo, a quantia grátis pode ser reduzida para 5 unidades se o saldo total for reduzido para 500 (por uma seqüência de perdas ou retirando saldo).


## Atribuir capital disponível:
Para utilizar totalmente os lucros compostos ao usar vários bots na mesma conta de câmbio, você deve limitar cada bot a um determinado saldo inicial. Isso pode ser feito definindo `available_capital` o equilíbrio inicial desejado.

Supondo que sua conta tenha 10.000 USDT e você deseja executar 2 estratégias diferentes nesta bolsa.

 Você `available_capital=5000` definiria - concedendo a cada bot um capital inicial de 5000 USDT. 

O bot irá então dividir este saldo inicial igualmente em baldes 'max_open_trades` . Negociações lucrativas resultarão em maiores valores de aposta para este bot - sem afetar os tamanhos de aposta do outro bot.

***

## Incompatível com `tradable_balance_ratio`!!

Definir esta opção substituirá qualquer configuração de `tradable_balance_ratio`.

***

## Alterar o último valor da aposta:

Assumindo que temos o saldo negociável de 1000 USDT `stake_amount=400`, e `max_open_trades=3`. O bot abriria 2 negociações e não conseguiria preencher o último slot de negociação, uma vez que os 400 USDT solicitados não estão mais disponíveis, uma vez que 800 USDT já estão empatados em outras negociações.


Para superar isso, a opção `amend_last_stake_amount` pode ser definida como `True` ,  o que permitirá ao bot reduzir o valor da aposta para o saldo disponível para preencher o último espaço de negociação.

No exemplo acima, isso significaria:

- Comércio1: 400 USDT
- Comércio2: 400 USDT
- Comércio 3: 200 USDT

**Observação-1 !!!**

Esta opção se aplica apenas ao valor da aposta estática - uma vez que o valor da aposta dinâmica divide os saldos igualmente.

**Observação-2 !!!**

valor mínimo da última aposta pode ser configurado usando `last_stake_amount_min_ratio` - cujo padrão é 0,5 (50%). Isso significa que o valor mínimo da aposta já usado é `stake_amount * 0.5`. Isso evita valores de aposta muito baixos, que estão próximos ao valor mínimo negociável do par e podem ser recusados ​​pela troca.
***
## Montante de aposta estática

A `stake_amount` configuração define estaticamente a quantidade de moeda da aposta que seu bot usará para cada negociação.

O valor mínimo de configuração é 0,0001, no entanto, verifique os mínimos de negociação da sua bolsa para a moeda da aposta que está usando para evitar problemas.

Esta configuração funciona em combinação com `max_open_trades`. O capital máximo envolvido nas negociações é `stake_amount * max_open_trades`. Por exemplo, o bot usará no máximo (0,05 BTC x 3) = 0,15 BTC, assumindo uma configuração de `max_open_trades=3` e `stake_amount=0.05`.

## Observação !!
Esta configuração respeita a [configuração de saldo disponível](https://www.freqtrade.io/en/stable/configuration/#available-balance) .
** 
## Montante da aposta dinâmica:

Alternativamente, você pode usar um valor de aposta dinâmica, que usará o saldo disponível na troca, e dividirá isso igualmente pelo número de negociações permitidas ( max_open_trades).

Para configurar isso, defina `stake_amount="unlimited"`. Também recomendamos definir `tradable_balance_ratio=0.99(99%)` - para manter um saldo mínimo para eventuais taxas.

**Neste caso, o valor da negociação é calculado como:**

```
currency_balance / (max_open_trades - current_open_trades)
```

Para permitir que o bot negocie todo o conjunto disponível `stake_currencyem` sua conta (menos `tradable_balance_ratio`)

```
"stake_amount" : "unlimited",
"tradable_balance_ratio": 0.99,
```
***
## Lucros compostos:
Esta configuração permitirá aumentar / diminuir as apostas dependendo do desempenho do bot (aposta mais baixa se o bot estiver perdendo, apostas mais altas se o bot tiver um histórico de vitórias, pois saldos mais altos estão disponíveis) e resultará na composição do lucro.

## Ao usar o modo de operação a seco!!:
Ao usar `"stake_amount" : "unlimited"`,em combinação com Dry-Run, Backtesting ou Hyperopt, o saldo será simulado começando com uma aposta `dry_run_wallet` que irá evoluir. Portanto, é importante definir `dry_run_wallet` um valor sensível (como 0,05 ou 0,01 para BTC e 1000 ou 100 para USDT, por exemplo), caso contrário, pode simular negociações com 100 BTC (ou mais) ou 0,05 USDT (ou menos) de uma vez - que pode não corresponder ao seu saldo real disponível ou é menor que o limite mínimo de câmbio para o valor do pedido para a moeda da aposta.


## Preços usados ​​para pedidos:
Os preços dos pedidos regulares podem ser controlados por meio das estruturas de parâmetros `bid_strategy` de compra e `ask_strategy` venda. Os preços são sempre obtidos imediatamente antes de um pedido ser feito, quer consultando os tickers da bolsa ou usando os dados da carteira de pedidos.



### Observação!!!
Os dados da carteira de pedidos usados ​​pelo Freqtrade são os dados recuperados da troca pela função do ccxt `fetch_order_book()`, ou seja, geralmente são dados da carteira de pedidos agregada L2, enquanto os dados do ticker são as estruturas retornadas pelas funções `fetch_ticker()`/ do ccxt `fetch_tickers()`. 
[Consulte a documentação](https://github.com/ccxt/ccxt/wiki/Manual#market-data) da biblioteca ccxt para obter mais detalhes.

Usando ordens de mercado













