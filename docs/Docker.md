# DOCKER-COMPOSE

### Início rápido do Docker¶

Crie um novo diretório e coloque o arquivo docker-compose neste diretório.
```
mkdir ft_userdata
cd ft_userdata/
# Download the docker-compose file from the repository
curl https://raw.githubusercontent.com/freqtrade/freqtrade/stable/docker-compose.yml -o docker-compose.yml

# Pull the freqtrade image
docker-compose pull

# Create user directory structure
docker-compose run --rm freqtrade create-userdir --userdir user_data

# Create configuration - Requires answering interactive questions
docker-compose run --rm freqtrade new-config --config user_data/config.json
```
O trecho acima cria um novo diretório chamado ft_userdata, baixa o arquivo de composição mais recente e puxa a imagem freqtrade. As últimas 2 etapas no snippet criam o diretório com user_data, bem como (interativamente) a configuração padrão com base em suas seleções.

***
## Como editar a configuração do bot?

Você pode editar a configuração a qualquer momento, que está disponível como user_data/config.json(no diretório ft_userdata) ao usar a configuração acima.

Você também pode alterar a Estratégia e os comandos editando a seção de comandos do seu docker-compose.yml arquivo.

**Adicionar uma estratégia personalizada:**

1. A configuração agora está disponível como user_data/config.json
2. Copie uma estratégia personalizada para o diretório user_data/strategies/
3. Adicione o nome da classe da Estratégia ao docker-compose.yml arquivo
O SampleStrategy é executado por padrão.

**SampleStrategy é apenas uma demonstração!**

 
    O SampleStrategyestá lá para sua referência e dar-lhe idéias para sua própria estratégia. Por favor, sempre faça backtest sua estratégia e use o ensaio por algum tempo antes de arriscar dinheiro real! Você encontrará mais informações sobre o desenvolvimento da Estratégia na  documentação da Estratégia .
    
# Docker Swarm:

- Repo_base [Repositorio do projeto](https://github.com/freqtrade/freqtrade)
- 01- [Principal Refrencia](https://www.omgthecloud.com/freqtrade-manage-a-fleet-of-crypto-bots/)
- 02- [Referencia Secundaria](https://www.omgthecloud.com/freqtrade-deploy-understanding-the-order-book/)

## Este é o docker compose:

***
```yml
version: '3.3'

networks:
  proxy-network:
    external: true

services:
  freqtrade:
    image: freqtradeorg/freqtrade:develop
    ports:
      - '8080:8080'
    volumes:
      - ./user_data:/freqtrade/user_data
      - /etc/timezone:/etc/timezone:ro
    command: >
      trade
      --logfile /freqtrade/user_data/logs/freqtrade.log
      --config /freqtrade/user_data/config.json
      --strategy ${STRATEGY}
      --db-url sqlite:///user_data/${DATABASE_FILE}
    networks:
      - proxy-network
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
      restart_policy:
        condition: on-failure
        delay: 5s
#      labels:
#        - 'traefik.http.routers.${BOTNAME}.tls=true'
#        - 'traefik.http.routers.${BOTNAME}.rule=Host(`${BOTNAME}.bots.yourdomain.com`)'
#        - 'traefik.http.services.${BOTNAME}.loadbalancer.server.port=8080'

```
Em seguida, você precisa do arquivo .env que contém as variáveis ​​referenciadas no exemplo docker-compose.yml , então aqui está!

```
BOTNAME=bot001
STRATEGY=BinHV45
DATABASE_FILE=trades.sqlite
```
O arquivo acima deve ser salvo como um “arquivo de ponto” oculto no mesmo caminho do arquivo docker- compose.yml , como .env
***
A variável BOTNAME determina qual será o nome totalmente qualificado do bot implantado, ou seja: https://bot001.bots.yourdomain.com
***
A variável STRATEGY precisa ser o nome exato (com distinção entre maiúsculas e minúsculas) do seu arquivo de estratégia, que é armazenado na pasta user_data/Strategies
***
Lembre-se também de que, quando estiver copiando sua pasta base FreqTrade (era bot001 no vídeo, por exemplo), você deseja entrar e remover o banco de dados trades.sqlite existente e limpar os logs removendo o logs / freqtrade.log arquivo se existir, após a cópia e antes da implantação
***
Por último, você provavelmente está procurando por aquele comando que permite usar variáveis .env com um contêiner implantado de implantação de pilha docker ! Aqui está:

```
docker stack deploy -c <(docker-compose config) stackName

```
Isso é semelhante ao comando de implantação padrão, no entanto, podemos ver que no lugar de onde normalmente referenciamos o arquivo docker-compose.yml , estamos dizendo a ele para avaliar essa configuração com docker-compose ! Quando executado, ele constrói a configuração, avaliando as variáveis ​​e, em seguida, passa tudo de volta para a implantação da pilha do docker para concluir a implantação.




# Deploy de nova aplicação:

mkdir ft_userdata
cd ft_userdata/
# Download the docker-compose file from the repository
curl https://raw.githubusercontent.com/freqtrade/freqtrade/stable/docker-compose.yml -o docker-compose.yml

# Pull the freqtrade image
docker-compose pull

# Create user directory structure
docker-compose run --rm freqtrade create-userdir --userdir user_data

# Create configuration - Requires answering interactive questions
docker-compose run --rm freqtrade new-config --config user_data/config.json
