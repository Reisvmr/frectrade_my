# pragma pylint: disable=missing-docstring, invalid-name, pointless-string-statement
# isort: skip_file
# --- Do not remove these libs ---
import numpy as np  # noqa
import pandas as pd  # noqa
from pandas import DataFrame

from freqtrade.strategy.interface import IStrategy

# --------------------------------
# Add your lib to import here
import talib.abstract as ta
import freqtrade.vendor.qtpylib.indicators as qtpylib

class BBRSIStrategy(IStrategy):
   # Versão da interface de estratégia - permite novas iterações da interface de estratégia.
    # Verifique a documentação ou a estratégia de amostra para obter a versão mais recente.
    INTERFACE_VERSION = 2

    # ROI mínimo projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "minimal_roi".
    minimal_roi = {
        "0": 0.21547444718127343,
        "21": 0.054918778723794665,
        "48": 0.013037720775643222,
        "125": 0
    }

    # Stoploss ideal projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "stoploss".
    stoploss = -0.3603667187598833

    # Trailing stoploss
    trailing_stop = False
    # trailing_only_offset_is_reached = False
    # trailing_stop_positive = 0.01
    # trailing_stop_positive_offset = 0.0  # Disabled / not configured

    # Optimal ticker interval for the strategy.
    timeframe = '15m'

    # Execute "populate_indicators ()" apenas para uma nova vela.
    process_only_new_candles = False

# Esses valores podem ser sobrescritos na seção "ask_strategy" na configuração.
    use_sell_signal = True
    sell_profit_only = False
    ignore_roi_if_buy_signal = False

    # Número de velas que a estratégia requer antes de produzir sinais válidos
    startup_candle_count: int = 30

    # Optional order type mapping.
    order_types = {
        'buy': 'limit',
        'sell': 'limit',
        'stoploss': 'market',
        'stoploss_on_exchange': False
    }

    # Optional order time in force.
    order_time_in_force = {
        'buy': 'gtc',
        'sell': 'gtc'
    }

    # plot_config = {
    #     'main_plot': {
    #         'tema': {},
    #         'sar': {'color': 'white'},
    #     },
    #     'subplots': {
    #         "MACD": {
    #             'macd': {'color': 'blue'},
    #             'macdsignal': {'color': 'orange'},
    #         },
    #         "RSI": {
    #             'rsi': {'color': 'red'},
    #         }
    #     }
    # }

    def informative_pairs(self):
        """
        Defina combinações adicionais e informativas de par / intervalo a serem armazenadas em cache da central.
        Estas combinações de par / intervalo não são negociáveis, a menos que façam parte
        da lista de permissões também.
        Para mais informações, consulte a documentação
        :return: List of tuples in the format (pair, interval)
            Sample: return [("ETH/USDT", "5m"),
                            ("BTC/USDT", "15m"),
                            ]
        """
        return []

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        # RSI
        dataframe['rsi'] = ta.RSI(dataframe)

        # Bollinger bands
        bollinger_1sd = qtpylib.bollinger_bands(qtpylib.typical_price(dataframe), window=20, stds=1)
        dataframe['bb_upperband_1sd'] = bollinger_1sd['upper']
        dataframe['bb_lowerband_1sd'] = bollinger_1sd['lower']

        bollinger_4sd = qtpylib.bollinger_bands(qtpylib.typical_price(dataframe), window=20, stds=4)
        dataframe['bb_lowerband_4sd'] = bollinger_4sd['lower']

        return dataframe

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                (dataframe['rsi'] > 25) &
                (dataframe['close'] < dataframe['bb_lowerband_1sd'])
            ),
            'buy'] = 1

        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                (dataframe['rsi'] > 95) &
                (dataframe['close'] > dataframe['bb_upperband_1sd'])
            ),
            'sell'] = 1

        return dataframe
