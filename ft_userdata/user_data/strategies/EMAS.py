# pragma pylint: disable = missing-docstring, invalid-name, pointless-string-statement
# flake8: noqa: F401
# isort: skip_file
# --- Não remova essas libs ---
import numpy as np  # noqa
import pandas as pd  # noqa
from pandas import DataFrame

from freqtrade.strategy import (BooleanParameter, CategoricalParameter, DecimalParameter,
                                IStrategy, IntParameter)

# --------------------------------
# Add your lib to import here
import talib.abstract as ta
import freqtrade.vendor.qtpylib.indicators as qtpylib

# Esta aula é um exemplo. Sinta-se à vontade para personalizá-lo.
class SMAOffsetReis(IStrategy):
    """
    Este é um exemplo de estratégia para inspirar você.
    Mais informações em https://www.freqtrade.io/en/latest/strategy-customization/

    Você pode:
        : return: um Dataframe com todos os indicadores obrigatórios para as estratégias
    - Renomeie o nome da classe (não se esqueça de atualizar class_name)
    - Adicione quaisquer métodos que você deseja construir sua estratégia
    - Adicione qualquer lib de que você precisa para construir sua estratégia

    Você deve manter:
    - the lib in the section "Do not remove these libs"
    - the methods: populate_indicators, populate_buy_trend, populate_sell_trend
    You should keep:
    - timeframe, minimal_roi, stoploss, trailing_*
    """
    # Versão da interface de estratégia - permite novas iterações da interface de estratégia.
    # Verifique a documentação ou a estratégia de amostra para obter a versão mais recente.
    #INTERFACE_VERSION = 2
    # ROI mínimo projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "minimal_roi".
    minimal_roi = {
        "1440": 0.0, # Venda quando a negociação não estiver perdendo (em vigor após 24h h) 
        #"720": 0.05, # Venda quando o lucro de 0,05% for alcançado (em vigor após 10h)
        #"300": 0.015,
        "0": 0.99
        #"0": 0.99 # - Venda sempre que o lucro de 99% for alcançado
        
    }

    """
    A configuração acima significaria, portanto:

    - Venda sempre que o lucro de 99% for alcançado
    - Venda quando o lucro de 60% for alcançado (em vigor após 20 minutos)
    - Venda quando o lucro de 1% for alcançado (em vigor após 30 minutos)
    - Venda quando a negociação não estiver perdendo (em vigor após 40 minutos)
    O cálculo inclui taxas.
    
    Para desativar o ROI completamente, defina-o para um número absurdamente alto:
    
    
    minimal_roi = {
        "0": 100
    }
    Embora tecnicamente não completamente desativado, isso seria vendido assim que a negociação atingir 10000% de lucro.
    """

    # Stoploss ideal projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "stoploss".
    stoploss = -0.05
    # Optimal timeframe for the strategy.
    timeframe = '15m'
    # Trailing stop
    # These attributes will be overridden if the config file contains corresponding values.
    trailing_stop = True
    trailing_stop_positive = 0.02001
    trailing_stop_positive_offset = 0.06038
    trailing_only_offset_is_reached = True

    # Hyperoptable parameters
    #buy_rsi = IntParameter(low=1, high=50, default=30, space='buy', optimize=True, load=True)
    #sell_rsi = IntParameter(low=50, high=100, default=70, space='sell', optimize=True, load=True)

    

    # Run "populate_indicators()" only for new candle.
    process_only_new_candles = False

    # These values can be overridden in the "ask_strategy" section in the config.
    use_sell_signal = True
    sell_profit_only = False
    ignore_roi_if_buy_signal = False

    # Number of candles the strategy requires before producing valid signals
    startup_candle_count: int = 30

    # Optional order type mapping.
    order_types = {
        'buy': 'limit',
        'sell': 'limit',
        'stoploss': 'market',
        'stoploss_on_exchange': False
    }

    # Optional order time in force.
    order_time_in_force = {
        'buy': 'gtc',
        'sell': 'gtc'
    }

    plot_config = {
        'main_plot1': {
            'ema8': {},
            'ema8': {'color': 'pink'},
        },
        'main_plot2': {
            'ema40': {},
            'ema40': {'color': 'black'},
        },
        'subplots': {
            "MACD": {
                'macd': {'color': 'blue'},
                'macdsignal': {'color': 'orange'},
            },
            "RSI": {
                'rsi': {'color': 'red'},
            }
        }
    }

    def informative_pairs(self):
        """
        Define additional, informative pair/interval combinations to be cached from the exchange.
        These pair/interval combinations are non-tradeable, unless they are part
        of the whitelist as well.
        For more information, please consult the documentation
        :return: List of tuples in the format (pair, interval)
            Sample: return [("ETH/USDT", "5m"),
                            ("BTC/USDT", "15m"),
                            ]
        """
        return []

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Adiciona vários indicadores TA diferentes ao DataFrame fornecido

        Nota de desempenho: Para obter o melhor desempenho, seja frugal no número de indicadores
        você está usando. Deixe descomentar apenas o indicador que você está usando em suas estratégias
        ou sua configuração hyperopt, caso contrário, você desperdiçará sua memória e uso de CPU.
        :param dataframe: Dataframe com dados da bolsa
        :param metadata: Informações adicionais, como o par atualmente negociado
        :return: Um Dataframe com todos os indicadores obrigatórios para as estratégias
        """

        # Momentum Indicators
        # ------------------------------------
        # # EMA - Exponential Moving Average
        dataframe['ema8'] = ta.EMA(dataframe, timeperiod=8)
        dataframe['ema40'] = ta.EMA(dataframe, timeperiod=40)
        # RSI
        dataframe['rsi'] = ta.RSI(dataframe)

        # Stochastic Fast
        #stoch_fast = ta.STOCHF(dataframe)
        #dataframe['fastd'] = stoch_fast['fastd']
        #dataframe['fastk'] = stoch_fast['fastk']

        # # Stochastic RSI
        # Please read https://github.com/freqtrade/freqtrade/issues/2961 before using this.
        # STOCHRSI is NOT aligned with tradingview, which may result in non-expected results.
        # stoch_rsi = ta.STOCHRSI(dataframe)
        # dataframe['fastd_rsi'] = stoch_rsi['fastd']
        # dataframe['fastk_rsi'] = stoch_rsi['fastk']

        # MACD
        #macd = ta.MACD(dataframe)
        #dataframe['macd'] = macd['macd']
        #dataframe['macdsignal'] = macd['macdsignal']
        #dataframe['macdhist'] = macd['macdhist']

        # Bollinger Bands
        #bollinger = qtpylib.bollinger_bands(qtpylib.typical_price(dataframe), window=20, stds=2)
        #dataframe['bb_lowerband'] = bollinger['lower']
        #dataframe['bb_middleband'] = bollinger['mid']
        #dataframe['bb_upperband'] = bollinger['upper']
        #dataframe["bb_percent"] = (
        #    (dataframe["close"] - dataframe["bb_lowerband"]) /
        #    (dataframe["bb_upperband"] - dataframe["bb_lowerband"])
        #)
        #dataframe["bb_width"] = (
        #    (dataframe["bb_upperband"] - dataframe["bb_lowerband"]) / dataframe["bb_middleband"]
        #)

        # Bollinger Bands - Weighted (EMA based instead of SMA)
        # weighted_bollinger = qtpylib.weighted_bollinger_bands(
        #     qtpylib.typical_price(dataframe), window=20, stds=2
        # )
        # dataframe["wbb_upperband"] = weighted_bollinger["upper"]
        # dataframe["wbb_lowerband"] = weighted_bollinger["lower"]
        # dataframe["wbb_middleband"] = weighted_bollinger["mid"]
        # dataframe["wbb_percent"] = (
        #     (dataframe["close"] - dataframe["wbb_lowerband"]) /
        #     (dataframe["wbb_upperband"] - dataframe["wbb_lowerband"])
        # )
        # dataframe["wbb_width"] = (
        #     (dataframe["wbb_upperband"] - dataframe["wbb_lowerband"]) /
        #     dataframe["wbb_middleband"]
        # )


        # # SMA - Simple Moving Average
        # dataframe['sma3'] = ta.SMA(dataframe, timeperiod=3)
        # dataframe['sma5'] = ta.SMA(dataframe, timeperiod=5)
        # dataframe['sma10'] = ta.SMA(dataframe, timeperiod=10)
        # dataframe['sma21'] = ta.SMA(dataframe, timeperiod=21)
        # dataframe['sma50'] = ta.SMA(dataframe, timeperiod=50)
        # dataframe['sma100'] = ta.SMA(dataframe, timeperiod=100)

        # TEMA - Triple Exponential Moving Average
        #dataframe['tema'] = ta.TEMA(dataframe, timeperiod=9)

        #
        # Pattern Recognition - Bullish candlestick patterns
        # ------------------------------------
        # # Hammer: values [0, 100]
        # dataframe['CDLHAMMER'] = ta.CDLHAMMER(dataframe)
        # # Inverted Hammer: values [0, 100]
        # dataframe['CDLINVERTEDHAMMER'] = ta.CDLINVERTEDHAMMER(dataframe)
        # # Dragonfly Doji: values [0, 100]
        # dataframe['CDLDRAGONFLYDOJI'] = ta.CDLDRAGONFLYDOJI(dataframe)
        # # Piercing Line: values [0, 100]
        # dataframe['CDLPIERCING'] = ta.CDLPIERCING(dataframe) # values [0, 100]
        # # Morningstar: values [0, 100]
        # dataframe['CDLMORNINGSTAR'] = ta.CDLMORNINGSTAR(dataframe) # values [0, 100]
        # # Three White Soldiers: values [0, 100]
        # dataframe['CDL3WHITESOLDIERS'] = ta.CDL3WHITESOLDIERS(dataframe) # values [0, 100]

        # Pattern Recognition - Bearish candlestick patterns
        # ------------------------------------
        # # Hanging Man: values [0, 100]
        # dataframe['CDLHANGINGMAN'] = ta.CDLHANGINGMAN(dataframe)
        # # Shooting Star: values [0, 100]
        # dataframe['CDLSHOOTINGSTAR'] = ta.CDLSHOOTINGSTAR(dataframe)
        # # Gravestone Doji: values [0, 100]
        # dataframe['CDLGRAVESTONEDOJI'] = ta.CDLGRAVESTONEDOJI(dataframe)
        # # Dark Cloud Cover: values [0, 100]
        # dataframe['CDLDARKCLOUDCOVER'] = ta.CDLDARKCLOUDCOVER(dataframe)
        # # Evening Doji Star: values [0, 100]
        # dataframe['CDLEVENINGDOJISTAR'] = ta.CDLEVENINGDOJISTAR(dataframe)
        # # Evening Star: values [0, 100]
        # dataframe['CDLEVENINGSTAR'] = ta.CDLEVENINGSTAR(dataframe)

        # Pattern Recognition - Bullish/Bearish candlestick patterns
        # ------------------------------------
        # # Three Line Strike: values [0, -100, 100]
        # dataframe['CDL3LINESTRIKE'] = ta.CDL3LINESTRIKE(dataframe)
        # # Spinning Top: values [0, -100, 100]
        # dataframe['CDLSPINNINGTOP'] = ta.CDLSPINNINGTOP(dataframe) # values [0, -100, 100]
        # # Engulfing: values [0, -100, 100]
        # dataframe['CDLENGULFING'] = ta.CDLENGULFING(dataframe) # values [0, -100, 100]
        # # Harami: values [0, -100, 100]
        # dataframe['CDLHARAMI'] = ta.CDLHARAMI(dataframe) # values [0, -100, 100]
        # # Three Outside Up/Down: values [0, -100, 100]
        # dataframe['CDL3OUTSIDE'] = ta.CDL3OUTSIDE(dataframe) # values [0, -100, 100]
        # # Three Inside Up/Down: values [0, -100, 100]
        # dataframe['CDL3INSIDE'] = ta.CDL3INSIDE(dataframe) # values [0, -100, 100]

        # # Chart type
        # # ------------------------------------
        # # Heikin Ashi Strategy
        # heikinashi = qtpylib.heikinashi(dataframe)
        # dataframe['ha_open'] = heikinashi['open']
        # dataframe['ha_close'] = heikinashi['close']
        # dataframe['ha_high'] = heikinashi['high']
        # dataframe['ha_low'] = heikinashi['low']

        # Retrieve best bid and best ask from the orderbook
        # ------------------------------------
        #"""
        ## first check if dataprovider is available
        #if self.dp:
        #    if self.dp.runmode.value in ('live', 'dry_run'):
        #        ob = self.dp.orderbook(metadata['pair'], 1)
        #        dataframe['best_bid'] = ob['bids'][0][0]
        #        dataframe['best_ask'] = ob['asks'][0][0]
        #"""

         return dataframe

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Com base nos indicadores  EMA, preenche o sinal de compra para o dataframe fornecido
        : param dataframe: DataFrame preenchido com indicadores
        : metadados param: informações adicionais, como o par atualmente negociado
        : return: DataFrame com coluna de compra
        """
        dataframe.loc[
            (
                (dataframe['rsi'], 30) & # Sinal: RSI ultrapassa 30
                (dataframe['ema8'] >= dataframe['ema40']) & # EMA8 ACIMA DA EMA40
                (dataframe['volume'] > 0)  # Certifique-se de que o volume não seja 0
            ),
            'buy'] = 1

        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Com base em indicadores de TA, preenche o sinal de venda para o dataframe fornecido
        : param dataframe: DataFrame preenchido com indicadores
        : metadados param: informações adicionais, como o par atualmente negociado
        : return: DataFrame com coluna de venda
        """
        dataframe.loc[
            (
                (dataframe['rsi'], 90) &  # Sinal: RSI cruza acima de 90
                (dataframe['ema9'] <= dataframe['ema40']) & # EMA 9 acima da EMA 40
                (dataframe['volume'] > 0)  # Certifique-se de que o volume não seja 0
            ),
            'sell'] = 1
        return dataframe
