# pragma pylint: disable=missing-docstring, invalid-name, pointless-string-statement
# isort: skip_file
# --- Do not remove these libs ---
import numpy as np  # noqa
import pandas as pd  # noqa
from pandas import DataFrame

from freqtrade.strategy.interface import IStrategy

# --------------------------------
# Add your lib to import here
import talib.abstract as ta
import freqtrade.vendor.qtpylib.indicators as qtpylib

class ReisStrategy(IStrategy):
   # Versão da interface de estratégia - permite novas iterações da interface de estratégia.
    # Verifique a documentação ou a estratégia de amostra para obter a versão mais recente.
    INTERFACE_VERSION = 2

    # ROI mínimo projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "minimal_roi".
    minimal_roi = {
        "1440": 0.0, # Venda quando a negociação não estiver perdendo (em vigor após 24h h)
        #"720": 0.02, # Venda quando o lucro de 0,05% for alcançado (em vigor após 12h)
        #"360": 0.05, # Venda quando o lucro de 0,15% for alcançado (em vigor após 6h)
        #"180": 0.10, # Venda quando o lucro de 0,10% for alcançado (em vigor após 2,30h)
        "90": 0.03, # Venda quando o lucro de 0,5% for alcançado (em vigor após 1h)
        "0": 0.10
    }

    # Stoploss ideal projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "stoploss".
    stoploss = -0.05
#### TESTE ###################
#########08:24-28-10-21
# Trailing stoploss
    trailing_stop = True
    trailing_only_offset_is_reached = True
    trailing_stop_positive = 0.01
    trailing_stop_positive_offset = 0.015
##############################
    # Trailing stoploss
    #trailing_stop = True
    #trailing_only_offset_is_reached = True #Nesse caso , o stoploss posterior é ativado apenas quando o deslocamento é alcançado.
    #trailing_stop_positive_offset = 0.00475  # Acione o stoploss positivo quando ultrapassar essa porcentagem
    #trailing_stop_positive = 0.00375 # Venda o ativo se ele cair tanto
    # trailing_only_offset_is_reached = False
    # trailing_stop_positive = 0.01
    # trailing_stop_positive_offset = 0.0  # Disabled / not configured

    # Optimal ticker interval for the strategy.
    timeframe = '60m'

    # Execute "populate_indicators ()" apenas para uma nova vela.
    process_only_new_candles = False

    # These values can be overridden in the "ask_strategy" section in the config.
    use_sell_signal = True
    sell_profit_only = False
    ignore_roi_if_buy_signal = False

    # Número de velas que a estratégia requer antes de produzir sinais válidos
    startup_candle_count: int = 200

    # Optional order type mapping.
    order_types = {
        'buy': 'limit',
        'sell': 'limit',
        'stoploss': 'market',
        'stoploss_on_exchange': False
    }

    # Optional order time in force.
    order_time_in_force = {
        'buy': 'gtc',
        'sell': 'gtc'
    }

    plot_config = {
        'main_plot': {
            'bb_upperband': {'color': 'blue'},
            'bb_midband': {'color': 'orange'},
            'bb_lowerband': {'color': 'blue'},
            'bb_lowerband': {'color': 'blue'},
            'ema8': {'color': 'pink'},
            'ema25': {'color': 'yellow'},
            'ema100': {'color': 'black'},
            'ema200': {'color': 'red'},
            
        },
        'subplots': {
            "MACD": {
                'macd': {'color': 'blue'},
                'macdsignal': {'color': 'orange'},
            },
            "RSI": {
                'rsi': {'color': 'yellow'},
            },
            #"stoch": {
            #    'stoch': {'color': 'blue'},
            #},
            #"stoch_fast": {
            #    'stoch_fast': {'color': 'blue'},
            #},

        }

    }

    def informative_pairs(self):
        """
        Defina combinações adicionais e informativas de par / intervalo a serem armazenadas em cache da central.
        Estas combinações de par / intervalo não são negociáveis, a menos que façam parte
        da lista de permissões também.
        Para mais informações, consulte a documentação
        :return: List of tuples in the format (pair, interval)
            Sample: return [("ETH/USDT", "5m"),
                            ("BTC/USDT", "15m"),
                            ]
        """
        return []

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        # RSI
        dataframe['rsi'] = ta.RSI(dataframe)
        # EMA - Exponential Moving Average
        dataframe['ema8'] = ta.EMA(dataframe, timeperiod=8)
        dataframe['ema25'] = ta.EMA(dataframe, timeperiod=25)
        dataframe['ema100'] = ta.EMA(dataframe, timeperiod=100)
        dataframe['ema200'] = ta.EMA(dataframe, timeperiod=200)
        # Bollinger bands
        bollinger = qtpylib.bollinger_bands(qtpylib.typical_price(dataframe), window=21, stds=2)
        dataframe['bb_upperband'] = bollinger['upper']
        dataframe['bb_midband'] = bollinger['mid']
        dataframe['bb_lowerband'] = bollinger['lower']

        # # Stochastic Slow
        stoch = ta.STOCH(dataframe)
        dataframe['slowd'] = stoch['slowd']
        dataframe['slowk'] = stoch['slowk']
        # MACD
        macd = ta.MACD(dataframe)
        dataframe['macd'] = macd['macd']
        dataframe['macdsignal'] = macd['macdsignal']
        dataframe['macdhist'] = macd['macdhist']
        # Stochastic RSI
        stoch = ta.STOCHRSI(dataframe, 21, 21, 3, 3)
        dataframe['srsi_fk'] = stoch['fastk']
        dataframe['srsi_fd'] = stoch['fastd']
        
        return dataframe

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                qtpylib.crossed_above(dataframe['ema8'], dataframe['ema25']) &
                (dataframe['rsi'] > 31) & # Sinal: RSI ultrapassa valor estipulado
                (dataframe['ema8'] > dataframe['ema100']) &
                (dataframe['ema25'] > dataframe['ema100']) &
                (dataframe['ema8'] > dataframe['ema200']) &
                (dataframe['ema25'] > dataframe['ema200']) &
                (dataframe['close'] < dataframe['bb_upperband']) &
                (dataframe['volume'] > 0)  # Certifique-se de que o volume não seja 0

            ),
            'buy'] = 1

        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                (dataframe['rsi'] > 60) & # Sinal: RSI é maior que 90
                (dataframe['close'] > dataframe['bb_upperband']) &
                (dataframe['volume'] > 0)  # Certifique-se de que o volume não seja 0
            ),
            'sell'] = 1

        return dataframe