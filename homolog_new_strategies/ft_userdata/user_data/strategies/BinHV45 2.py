"""
Modificação roi
"""

# --- Do not remove these libs ---
from freqtrade.strategy.interface import IStrategy
from typing import Dict, List
from functools import reduce
from pandas import DataFrame
import numpy as np
# --------------------------------

import talib.abstract as ta
import freqtrade.vendor.qtpylib.indicators as qtpylib


def bollinger_bands(stock_price, window_size, num_of_std):
    rolling_mean = stock_price.rolling(window=window_size).mean()
    rolling_std = stock_price.rolling(window=window_size).std()
    lower_band = rolling_mean - (rolling_std * num_of_std)

    return rolling_mean, lower_band


class BinHV45(IStrategy):
    minimal_roi = {
      "40": 0.0,    # Sell after 40 minutes if the profit is not negative
      "30": 0.01,   # Sell after 30 minutes if there is at least 1% profit
      "20": 0.02,   # Sell after 20 minutes if there is at least 2% profit
      "0":  0.04    # Sell immediately if there is at least 4% profit
    #    "0": 0.0125
      "0": 0.9
    }
    """
Trailing stop loss apenas quando o comércio atingiu um certo deslocamento
Também é possível usar um stoploss estático até que o 
deslocamento seja alcançado e,em seguida, rastrear a negociação para obter 
lucros quando o mercado virar.

"trailing_only_offset_is_reached": trueNesse caso , 
o stoploss posterior é ativado apenas quando o deslocamento é alcançado. 
Até então, o stoploss permanece no configurado stoploss. 
Esta opção pode ser usada com ou sem trailing_stop_positive, 
mas usa trailing_stop_positive_offsetcomo deslocamento.

    """
    stoploss = -0.010
    timeframe = '1m'
    trailing_stop = True
    trailing_only_offset_is_reached = True #Nesse caso , o stoploss posterior é ativado apenas quando o deslocamento é alcançado.
    trailing_stop_positive_offset = 0.00475  # Acione o stoploss positivo quando ultrapassar essa porcentagem
    trailing_stop_positive = 0.00375 # Venda o ativo se ele cair tanto
    # Alteração 
    # Original
    # trailing_stop_positive = 0.00275
    # trailing_stop_positive_offset = 0.00375
    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        mid, lower = bollinger_bands(dataframe['close'], window_size=40, num_of_std=2)
        dataframe['mid'] = np.nan_to_num(mid)
        dataframe['lower'] = np.nan_to_num(lower)
        dataframe['bbdelta'] = (dataframe['mid'] - dataframe['lower']).abs()
        dataframe['pricedelta'] = (dataframe['open'] - dataframe['close']).abs()
        dataframe['closedelta'] = (dataframe['close'] - dataframe['close'].shift()).abs()
        dataframe['tail'] = (dataframe['close'] - dataframe['low']).abs()
        return dataframe

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                dataframe['lower'].shift().gt(0) &
                dataframe['bbdelta'].gt(dataframe['close'] * 0.008) &
                dataframe['closedelta'].gt(dataframe['close'] * 0.0175) &
                dataframe['tail'].lt(dataframe['bbdelta'] * 0.25) &
                dataframe['close'].lt(dataframe['lower'].shift()) &
                dataframe['close'].le(dataframe['close'].shift())
            ),
            'buy'] = 1
        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        no sell signal
        """
        dataframe.loc[:, 'sell'] = 0
        return dataframe
