# pragma pylint: disable=missing-docstring, invalid-name, pointless-string-statement
# isort: skip_file
# --- Do not remove these libs ---
import numpy as np  # noqa
import pandas as pd  # noqa
from pandas import DataFrame

from freqtrade.strategy.interface import IStrategy

# --------------------------------
# Add your lib to import here
import talib.abstract as ta
import freqtrade.vendor.qtpylib.indicators as qtpylib

class BBRSINaiveStrategy(IStrategy):
   # Versão da interface de estratégia - permite novas iterações da interface de estratégia.
    # Verifique a documentação ou a estratégia de amostra para obter a versão mais recente.
    INTERFACE_VERSION = 2

    # ROI mínimo projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "minimal_roi".
    minimal_roi = {
        "1440": 0.0, # Venda quando a negociação não estiver perdendo (em vigor após 24h h) 
        #"720": 0.05, # Venda quando o lucro de 0,05% for alcançado (em vigor após 10h)
        #"300": 0.015,
        "0": 0.99
    }

    # Stoploss ideal projetado para a estratégia.
    # Este atributo será sobrescrito se o arquivo de configuração contiver "stoploss".
    stoploss = -0.05

    # Trailing stoploss
    trailing_stop = False
    # trailing_only_offset_is_reached = False
    # trailing_stop_positive = 0.01
    # trailing_stop_positive_offset = 0.0  # Disabled / not configured

    # Optimal ticker interval for the strategy.
    timeframe = '15m'

    # Execute "populate_indicators ()" apenas para uma nova vela.
    process_only_new_candles = False

    # These values can be overridden in the "ask_strategy" section in the config.
    use_sell_signal = True
    sell_profit_only = False
    ignore_roi_if_buy_signal = False

    # Número de velas que a estratégia requer antes de produzir sinais válidos
    startup_candle_count: int = 30

    # Optional order type mapping.
    order_types = {
        'buy': 'limit',
        'sell': 'limit',
        'stoploss': 'market',
        'stoploss_on_exchange': False
    }

    # Optional order time in force.
    order_time_in_force = {
        'buy': 'gtc',
        'sell': 'gtc'
    }

    plot_config = {
        'main_plot': {
            'bb_upperband': {'color': 'green'},
            'bb_midband': {'color': 'orange'},
            'bb_lowerband': {'color': 'red'},
        },
        'subplots': {
            "RSI": {
                'rsi': {'color': 'yellow'},
            }
        }
    }

    def informative_pairs(self):
        """
        Defina combinações adicionais e informativas de par / intervalo a serem armazenadas em cache da central.
        Estas combinações de par / intervalo não são negociáveis, a menos que façam parte
        da lista de permissões também.
        Para mais informações, consulte a documentação
        :return: List of tuples in the format (pair, interval)
            Sample: return [("ETH/USDT", "5m"),
                            ("BTC/USDT", "15m"),
                            ]
        """
        return []

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        # RSI
        dataframe['rsi'] = ta.RSI(dataframe)
        # EMA - Exponential Moving Average
        dataframe['ema8'] = ta.EMA(dataframe, timeperiod=8)
        dataframe['ema40'] = ta.EMA(dataframe, timeperiod=40)

        # Bollinger bands
        #bollinger = qtpylib.bollinger_bands(qtpylib.typical_price(dataframe), window=20, stds=2)
        #dataframe['bb_upperband'] = bollinger['upper']
        #dataframe['bb_midband'] = bollinger['mid']
        #dataframe['bb_lowerband'] = bollinger['lower']

        return dataframe

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                (dataframe['rsi'] > 30) &   # Sinal: RSI ultrapassa 30
                (dataframe['ema8'] > dataframe['ema40']) #  # Sinal: RSI ultrapassa 30
            ),
            'buy'] = 1

        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                (dataframe['rsi'] > 90) & # Sinal: RSI é maior que 90
                (dataframe['ema8'] < dataframe['ema40']) # Sinal: o preço é maior que mid bb
            ),
            'sell'] = 1

        return dataframe